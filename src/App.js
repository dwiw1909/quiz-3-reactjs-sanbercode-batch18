import React from 'react';
import './App.css';
import Routes from './Routes/Routes'
import Store from './Store'

function App() {
  return (
    <Store>
      <Routes />
    </Store>
  );
}

export default App;
