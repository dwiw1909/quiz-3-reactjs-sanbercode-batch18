import React, {useState, createContext} from 'react'

export const Context = createContext()

const Store = ({children}) => {
  const [user, setUser] = useState({name: ''})
  const [movies, setMovies] = useState([])
  return (
    <Context.Provider value={[user, setUser, movies, setMovies]}>
      {children}
    </Context.Provider>
  )
}

export default Store
