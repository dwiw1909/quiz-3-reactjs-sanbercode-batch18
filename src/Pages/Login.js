import React, {useState, useContext} from 'react'

import {Context} from '../Store'

const Login = () => {
  const [, setUser] = useContext(Context)
  const [form, setForm] = useState({user: '', password: ''})

  const handleForm = (type, val) => {
    setForm({
      ...form,
      [type]: val
    })
  }

  const Login = (e) => {
    e.preventDefault()
    if(form.user === 'admin' && form.password === 'admin'){
      console.log('login berhasil');
      setUser({name: 'admin'})
      localStorage.setItem('user', JSON.stringify({name: 'admin'}))
    }
  }

  return (
    <form onSubmit={Login} style={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '10px'}}>
      <div>
        <label>Username: </label>
        <input type="text" value={form.user || ''} onChange={value => handleForm('user', value.target.value)}/>
      </div>
      <div>
        <label>Password: </label>
        <input type="password" value={form.password || ''} onChange={value => handleForm('password', value.target.value)}/><br /> 
      </div>
      <input type="submit" value="Login" />
    </form> 
  )
}

export default Login;