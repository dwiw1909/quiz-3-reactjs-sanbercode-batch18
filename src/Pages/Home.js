import React, {useContext} from 'react'
import MovieItem from '../Components/MovieItem'
import {Context} from '../Store'

const Home = () => {
  const [, , movies] = useContext(Context)

  return (
    <div>
      <h1 style={{textAlign: 'center'}}>Daftar Film-Film Terbaik</h1>
      {movies.map((data) => (
        <MovieItem data={data} key={data.id} />
      ))}
    </div>
  )
}

export default Home

