import React from 'react'

const About = () => {
  return (
    <div style={{border: '1px solid gray', padding: '30px'}}>
      <h1 style={{textAlign: 'center'}}>Data Peserta Sanbercode Bootcamp ReactJS</h1>
      <br />
      <p>1. <b>Name: </b>Dwi Wicaksono</p>
      <p>2. <b>Email: </b>dwiw1909@gmail.com</p>
      <p>3. <b>Sistem Operasi yang digunakan: </b>macOS</p>
      <p>4. <b>Akun Gitlab: </b>@dwiw1909</p>
      <p>5. <b>Akun Telegram: </b>@nndiw</p>
    </div>
  )
}

export default About
