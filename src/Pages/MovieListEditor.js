import React, {useContext, useState} from 'react'
import Axios from 'axios'

import {Context} from '../Store'
import ListMoviesEditor from '../Components/ListMoviesEditor'
import './MovieListEditor.css'


const MovieListEditor = () => {
  const [,,movies, setMovies] = useContext(Context)
  let inp = {title: '', description: '', year: '2020', duration: 120, genre: '', rating: 0, image_url: '' }
  const [form, setForm] = useState(inp)
  const [edit, setEdit] = useState(false)
  const [searchForm, setSearchForm] = useState('')

  const handleForm = (key, val) => {
    setForm({
      ...form,
      [key] : val
    })
  }

  const search = () => {
    if(searchForm === ''){
      fetchDatas()
    } else {
      Axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(({data}) => {
        setMovies(data.filter(({title}) => {
          return title.toLowerCase().indexOf(searchForm.toLowerCase()) !== -1
        }))
      }).catch((err) => {
        console.log(err)
      });
    }
  }

  const fetchDatas = () => {
    Axios.get(`http://backendexample.sanbercloud.com/api/movies`)
    .then(({data}) => {
      setMovies(data)
    }).catch((err) => {
      console.log(err)
    });
  }

  const submitForm = (e) => {
    e.preventDefault();
    if(form.year < 1980){
      alert('minimal tahun 1980')
    }else if(form.rating < 0 || form.rating > 10){
      alert('rating dari 0 - 10')
    } else if(!edit){
      console.log(form);
      Axios.post(`http://backendexample.sanbercloud.com/api/movies`, form)
      .then((result) => {
        console.log('berhasil save');
        setForm(inp)
        fetchDatas()
      }).catch((err) => {
        console.log(err);
      });
    } else if(edit){
        Axios.put(`http://backendexample.sanbercloud.com/api/movies/${form.id}`, form)
        .then((result) => {
          console.log('berhasil edit');
          fetchDatas()
          setForm(inp)
          setEdit(false)
        }).catch((err) => {
          console.log(err)
        });
    }
  }

  const delMov = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
    .then((result) => {
      fetchDatas()
    }).catch((err) => {
      console.log(err)
    });
  }

  const handleEdit = (id) => {
    setEdit(true)
    Axios.get(`http://backendexample.sanbercloud.com/api/movies/${id}`)
    .then(({data}) => {
      setForm(data)
    }).catch((err) => {
      console.log(err)
    });
  }

  return (
    <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'ceter', alignItems: 'center'}}>
      <div>
        <input type='text' value={searchForm} onChange={value => setSearchForm(value.target.value)} />
        <button onClick={search} type='button'>Search</button>
      </div>
      <div>
        <h1 style={{textAlign: 'center'}}>Daftar Film</h1>
        <table className='tableListEditor'>
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Description</th>
              <th>Year</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {movies.map((data, index) => (<ListMoviesEditor data={data} key={data.id} no={index+1} del={() => delMov(data.id)} edit={() => handleEdit(data.id)} />))}
          </tbody>
        </table>
      </div>
      
      <div>
        <h1 style={{textAlign: 'center'}}>Movies Form</h1>
        <form onSubmit={submitForm} style={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '10px'}}>
          <div className='formList'>
            <label>Title: </label>
            <input required type="text" value={form.title || ''} onChange={value => handleForm('title', value.target.value)}/>
          </div>
          <div className='formList'>
            <label>Description: </label>
            {/* <input required type="text" value={form.description || ''} onChange={value => handleForm('description', value.target.value)}/><br />  */}
            <textarea required  rows="2" cols="30" value={form.description || ''} onChange={value => handleForm('description', value.target.value)}></textarea>
          </div>
          <div className='formList'>
            <label>Year: </label>
            <input required type="text" value={form.year || ''} onChange={value => handleForm('year', value.target.value)}/>
          </div>
          <div className='formList'>
            <label>Duration: </label>
            <input required type="text" value={form.duration || ''} onChange={value => handleForm('duration', value.target.value)}/>
          </div>
          <div className='formList'>
            <label>Genre: </label>
            <input required type="text" value={form.genre || ''} onChange={value => handleForm('genre', value.target.value)}/>
          </div>
          <div className='formList'>
            <label>Rating: </label>
            <input required type="text" value={String(form.rating) || ''} onChange={value => handleForm('rating', value.target.value)}/>
          </div>
          <div className='formList'>
            <label>Image Url: </label>
            <textarea required  rows="3" cols="30" value={form.image_url || ''} onChange={value => handleForm('image_url', value.target.value)}></textarea>
            {/* <input required type="text" value={form.image_url || ''} onChange={value => handleForm('image_url', value.target.value)}/> */}
          </div>
          
          <input type="submit" value="Submit" />
        </form> 
      </div>
    </div>
  )
}

export default MovieListEditor
