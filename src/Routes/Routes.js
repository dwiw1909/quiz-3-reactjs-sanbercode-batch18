import React, {useContext} from 'react'
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect
} from 'react-router-dom'

import NavBar from '../Components/NavBar'
import About from '../Pages/About'
import Home from '../Pages/Home'
import Login from '../Pages/Login'
import MovieListEditor from '../Pages/MovieListEditor'
import {Context} from '../Store'

const Routes = () => {
  const [user] = useContext(Context)
  return (
    <Router>
      <NavBar />
      <div style={{width: '70%', backgroundColor: 'white', margin: '10px auto', padding: '20px'}}>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' component={About} />
          <Route path='/login'>{user.name ? <Redirect to='/'/> : <Login /> }</Route>
          <Route path='/movielisteditor'>{user.name ? <MovieListEditor /> : <Redirect to='/login'/>}</Route>
        </Switch>
      </div>
    </Router>
  )
}

export default Routes
