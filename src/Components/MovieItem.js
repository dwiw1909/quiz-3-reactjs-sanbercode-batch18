import React from 'react'

const MovieItem = ({data}) => {
  return (
    <div>
      <h2>{data.title}</h2>
      <div style={{display: "flex", flexDirection: 'row'}}>
        <img style={{height: '300px', width: '500px', objectFit: 'cover'}} src={data.image_url} alt={`foto ${data.title}`} />
        <div style={{marginLeft: '10px'}}>
          <h4>Rating {data.rating}</h4>
          <h4>Durasi: {data.duration} Menit</h4>
          <h4>Genre: {data.genre}</h4>
        </div>
      </div>
      <p><b>Deskripsi: </b>{data.description}</p>
      <br />
      <hr />
    </div>
  )
}

export default MovieItem
