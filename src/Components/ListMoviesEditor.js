import React from 'react'

const ListMoviesEditor = ({data, no, del, edit}) => {
  // const desc = () => {
  //   if(data.description !== '' && data.description.length > 20){
  //     return data.description.slice(0, 20) + ' ...'
  //   } else {
  //     return data.description
  //   }
  // }

  return (
    <tr>
      <td style={{width: '30px', textAlign: 'center'}}>{no}</td>
      <td style={{width: '200px'}}>{data.title}</td>
      <td style={{width: '150px'}}>{data.description && data.description.slice(0, 20) + '...'}</td>
      <td style={{width: '30px', textAlign: 'center'}}>{data.year}</td>
      <td style={{width: '50px', textAlign: 'center' }}>{data.duration}</td>
      <td style={{width: '30px', textAlign: 'center'}}>{data.genre}</td>
      <td style={{width: '30px', textAlign: 'center'}}>{data.rating}</td>
      <td style={{width: '30px', textAlign: 'center'}}>
        <button onClick={edit} type='button'>Edit</button>
        <button onClick={del} type='button'>Delete</button>
      </td>
    </tr>
  )
}

export default ListMoviesEditor
