import React, {useContext, useEffect} from 'react'
import {Link} from 'react-router-dom'
import './NavBar.css'
import {Context} from '../Store'
import Axios from 'axios'

const NavBar = () => {
  const [user, setUser, , setMovies] = useContext(Context)

  useEffect(() => {
    fetchDatas()
    setUser(JSON.parse(localStorage.getItem('user')) || '') //cek login
  }, [])

  const logout = () => {
    localStorage.removeItem('user');
    setUser({name: ''})
  }

  const fetchDatas = () => {
    Axios.get(`http://backendexample.sanbercloud.com/api/movies`)
    .then(({data}) => {
      console.log(data);
      setMovies(data)
    }).catch((err) => {
      console.log(err)
    });
  }

  return (
    <div className='nav'>
      <ul>
        <li><img src='https://sanbercode.com/assets/img/identity/logo-horizontal.svg' style={{width: '100px', height: '50px'}} alt='logo Sanber' /></li>
        
        <li style={{float: 'right', padding: '14px 16px', color: 'white'}}>
          <ul>
            <li><Link to='/' className='a'>Home</Link></li>
            {user.name && <li><Link to='/movielisteditor' className='a'>Movie List Editor</Link></li>}
            <li><Link to='/about' className='a'>About</Link></li>
            {
              !user.name 
              ? <li><Link to='/login' className='a'>Login</Link></li>
              : <li><button onClick={logout} className='a'>Logout</button></li>
            }
          </ul>
        </li>
      </ul>
    </div>
  )
}

export default NavBar
